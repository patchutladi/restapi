package runner;

import cucumber.api.CucumberOptions;
import net.serenitybdd.cucumber.CucumberWithSerenity;
import org.junit.runner.RunWith;

@RunWith(CucumberWithSerenity.class)
@CucumberOptions(features = { "src/main/resources/feature_files"},
//@CucumberOptions(features = { "src/test/resources/features" },
        glue = { "src/main/java/stepdefinitions" } )
public class Runner {
}
