Feature: Pet Store
  As a user I would like to perform functions for the pet service


  Scenario Outline: Retrieve all available pets and confirm that the name “doggie” with category id "12" is on the list
    Given that user has endpoint to invoke available pets
    When user searches for the "<name>" and with "<category>"
    Then endpoint returns all available pets with "<name>" and with "<category>"
    Examples:
      | name   | category |
      | doggie | 12       |


  Scenario: Add a new pet with an auto generated name and status available - Confirm the new pet has been added
    Given that user has endpoint to invoke available pets
    When user adds a new pet with an auto generated name and status available
    Then the new pet has been added
    And retrieve the created pet using the ID



