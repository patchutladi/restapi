 Feature: Dog Store
As a user I would like to perform functions for the dog service

@dogbreeds
   Scenario: Verify that a successful message is returned when a user searches for random breeds
   Given that user has endpoint to invoke random breeds
     Then endpoint returns successful message

   Scenario: Verify that bulldog is on the list of all breeds
     Given that user has endpoint to invoke random breeds
     When user searches for bulldog on the list
     Then bulldog is on the list of all breeds


   Scenario:  Retrieve all sub-breeds for bulldogs and their respective images
     Given that user has endpoint to invoke random breeds
     When user searches sub-breeds for bulldogs and their respective images
     Then all sub-breeds for bulldogs and their respective images



