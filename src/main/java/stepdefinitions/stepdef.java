package stepdefinitions;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import services.Implementations;

public class stepdef {

    Implementations implementations = new Implementations();
    @Given("^that user has endpoint to invoke random breeds$")
    public void that_user_has_endpoint_to_invoke_random_breeds() throws Exception {
        // Write code here that turns the phrase above into concrete actions
        implementations.callToGetAllBreeds();
    }

    @Then("^endpoint returns successful message$")
    public void endpoint_returns_successful_message() throws Exception {
        // Write code here that turns the phrase above into concrete actions
        implementations.verifyBreedResponse();
    }

    @When("^user searches for bulldog on the list$")
    public void user_searches_for_bulldog_on_the_list() throws Exception {
        // Write code here that turns the phrase above into concrete actions
        implementations.searchBullDog();
    }

    @Then("^bulldog is on the list of all breeds$")
    public void bulldog_is_on_the_list_of_all_breeds() throws Exception {
        // Write code here that turns the phrase above into concrete actions
    
    }

    @When("^user searches sub-breeds for bulldogs and their respective images$")
    public void user_searches_sub_breeds_for_bulldogs_and_their_respective_images() throws Exception {
        // Write code here that turns the phrase above into concrete actions
      implementations.searchBullDogSubBreeds();
    }

    @Then("^all sub-breeds for bulldogs and their respective images$")
    public void all_sub_breeds_for_bulldogs_and_their_respective_images() throws Exception {
        // Write code here that turns the phrase above into concrete actions
    
    }

    @Given("^that user has endpoint to invoke available pets$")
    public void that_user_has_endpoint_to_invoke_available_pets() throws Exception {
        // Write code here that turns the phrase above into concrete actions
         implementations.getAllPets();
    }

    @When("^user searches for the \"([^\"]*)\" and with \"([^\"]*)\"$")
    public void user_searches_for_the_and_with(String petName, String petCategory) throws Exception {
        // Write code here that turns the phrase above into concrete actions
        implementations.searchPets(petName,petCategory);
    }

    @Then("^endpoint returns all available pets with \"([^\"]*)\" and with \"([^\"]*)\"$")
    public void endpoint_returns_all_available_pets_with_and_with(String arg1, String arg2) throws Exception {
        // Write code here that turns the phrase above into concrete actions
      
    }

    @When("^user adds a new pet with an auto generated name and status available$")
    public void user_adds_a_new_pet_with_an_auto_generated_name_and_status_available() throws Exception {
        // Write code here that turns the phrase above into concrete actions
       implementations.addPet();
    }

    @Then("^the new pet has been added$")
    public void the_new_pet_has_been_added() throws Exception {
        // Write code here that turns the phrase above into concrete actions
    
    }

    @Then("^retrieve the created pet using the ID$")
    public void retrieve_the_created_pet_using_the_ID() throws Exception {
        // Write code here that turns the phrase above into concrete actions
            implementations.getPetByID();
    }

}
