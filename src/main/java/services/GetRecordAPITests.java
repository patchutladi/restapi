/*
Project:MFC Non Dealer Direct
Author: Miss P
Purpose of Class: Sends Application retrieving requests
 */
package services;

import io.restassured.response.Response;
import model.GlobalFunctions;
import net.serenitybdd.rest.SerenityRest;
import net.thucydides.core.annotations.Step;
import org.json.JSONObject;

import static org.hamcrest.Matchers.equalTo;

public class GetRecordAPITests {
    private Response response;
    private GlobalFunctions globalVariables;

    public GetRecordAPITests()
    {
        globalVariables = new GlobalFunctions();
    }

    public Response callGetAllBreeds(String endpoint) {
        response = SerenityRest.given().relaxedHTTPSValidation()
                .when()
                .get( globalVariables.dogEndpoint + endpoint);
        return response;
    }
    public Response callGetAllPets(String endpoint) {
        response = SerenityRest.given().relaxedHTTPSValidation()
                .when()
                .get( globalVariables.petEndpoint + endpoint);
        return response;
    }


    public Response callAddPet(String endpoint, String petObject) {
        response = SerenityRest.given().relaxedHTTPSValidation()
                .when()
                .body(petObject)
                .when()
                .post( globalVariables.petEndpoint + endpoint);
        return response;
    }


}
