package services;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.restassured.response.ResponseBody;
import model.PetObject;
import  org.junit.Assert;



import java.io.IOException;

public class Implementations {

    String breedResponse;
    GetRecordAPITests getRecordAPITests = new GetRecordAPITests();
    public void callToGetAllBreeds()
    {
        breedResponse = getRecordAPITests.callGetAllBreeds("/breeds/list/all").getBody().jsonPath().getString("status");
        System.out.println(breedResponse);

    }
    public void verifyBreedResponse()
    {
        if(breedResponse.contains("success"))
        {
            Assert.assertTrue("All breeds were returned", true);
        }
        else
        {
            Assert.assertFalse("All breeds were returned", true);
        }
    }

    public void searchBullDog()
    {
        breedResponse = getRecordAPITests.callGetAllBreeds("/breeds/list/all").getBody().jsonPath().getString("message.bulldog");
        System.out.println(breedResponse);
    }

    public void searchBullDogSubBreeds()
    {
        breedResponse = getRecordAPITests.callGetAllBreeds("/breed/bulldog/images").getBody().jsonPath().getString("message");
        System.out.println(breedResponse);
    }

    public ResponseBody getAllPets()
    {
        return getRecordAPITests.callGetAllPets("pet/findByStatus?status=available").getBody();
    }
    public void searchPets(String petName, String petCategory) throws IOException {

        String jsonString = getAllPets().asString();
        ObjectMapper mapper = new ObjectMapper();
        PetObject[] asMap = mapper.readValue(jsonString, PetObject[].class);

        for(PetObject dogName:asMap)
        {
            if(dogName.name.equals(petName) && dogName.category.id == 12)
            {
                Assert.assertTrue("Found a match.",true);
            }
            else
            {
                Assert.assertFalse("Unable to find a match.", false);
            }
        }

    }

    public void  addPet() throws JsonProcessingException {
           ObjectMapper mapper = new ObjectMapper();
           PetObject petObjectModel = new PetObject();
           String petObject = mapper.writeValueAsString(petObjectModel);
           System.out.println(petObject.toString());
           getRecordAPITests.callAddPet("pet", petObject ).getBody();
    }
    public void  getPetByID() throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        PetObject petObjectModel = new PetObject();
        String petID  = String.valueOf(petObjectModel.id);
       if(!getRecordAPITests.callGetAllPets("pet/" + petID.toString()).getBody().equals(null))
        {
            Assert.assertTrue("Pet ID found",true);
        }
       else{
            Assert.assertFalse("Pet ID not found", false);
        }
    }

}
