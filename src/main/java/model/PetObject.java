package model;

import com.github.javafaker.Faker;

import java.util.ArrayList;

public class PetObject {

    public int id;
    public  PetCategoryObject category;
    public String name;
    public ArrayList photoUrls = new ArrayList();
    public String status;
    public ArrayList<PetPhotoTags> tags = new ArrayList<>();

    public PetObject()
    {
        this.id = 23;
        Faker faker = new Faker();
        this.name = faker.name().firstName();
        this.status = "available";
        category = new PetCategoryObject();
        category.id = 123;
        category.name = "testing";


    }

}
