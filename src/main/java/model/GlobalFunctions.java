package model;

import net.serenitybdd.rest.SerenityRest;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class GlobalFunctions {

    Properties prop;
    FileInputStream  input;

    public  String dogEndpoint = "";
    public  String petEndpoint = "";
    public GlobalFunctions()
    {
        prop = new Properties();

        try {
            input = new FileInputStream("src/main/resources/env/application.properties");
            prop.load(input);
            dogEndpoint = prop.getProperty("applicationDog");
            petEndpoint = prop.getProperty("applicationPet");

        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
}
